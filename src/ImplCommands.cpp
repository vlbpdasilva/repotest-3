
#include "ImplCommands.h"

#include "PlatformPins.h"
#include "IPeriodicCommandDispatcher.h"

#include <mbed.h>
#include <ArduinoJson.h>
#include <eeprom.h>

using namespace ArduinoJson;

using namespace core;

#define USE_EFM32GG_FLASH_FOR_PLAT_ID   //If platform board doesn't have EEPROM on it, uncomment
#ifdef USE_EFM32GG_FLASH_FOR_PLAT_ID
#include "efm32gg.h"
#else // USE_EFM32GG_FLASH_FOR_PLAT_ID
#include "cat24c512.h"
#endif // USE_EFM32GG_FLASH_FOR_PLAT_ID

I2C i2c_1(I2C1_SDA_PIN, I2C1_SCL_PIN);          /*!< I2C bus controlling temperature probe, eeprom, and digital pots  */
I2C *i2c1 = &i2c_1;                             /*!< Pointer for I2C-1 */

#ifdef USE_EFM32GG_FLASH_FOR_PLAT_ID
    Efm32gg efm32gg(NULL, 0, 0, 0);
    EEPROM *eeprom = &efm32gg;
#else // USE_EFM32GG_FLASH_FOR_PLAT_ID
    // EEPROM device
    Cat24c512 cat24c512(i2c1, CAT24C512_I2C_ADDR<<1, CAT24C512_CHIP_SIZE);
    #define PAGE_SIZE 2048
    // EEPROM interface pointer
    EEPROM *eeprom = &cat24c512;
#endif // USE_EFM32GG_FLASH_FOR_PLAT_ID

//Initialize I/O pins
DigitalOut link_led(LINK_LED_PIN);  // link LED directly on waterheater, need function to enable this when Strata is connected

/*!
 * Commands registration
 */

ImplCommands::ImplCommands(dispatcher::IPeriodicCommandDispatcher *dispatcher)
    : commands::CoreCommands(dispatcher), dispatcher_(dispatcher)
{
    // Setup handler for "example_command" when its received
    dispatcher_->addCommandHandler(
        "example_command",
        std::bind(&ImplCommands::exampleCommand, this, std::placeholders::_1));

    // Setup period notification every 500ms ( 2Hz )
    dispatcher_->addPeriodicHandler(
        "example_periodic_command",
        std::bind(&ImplCommands::examplePeriodicCommand, this), 500, -1, true);

    // Blink the link LED every 200ms
    dispatcher_->addPeriodicHandler(
        "blink_link_led", std::bind(&ImplCommands::blinkLinkLed, this), 200, -1, true);

}

//////////////////////// ON DEMAND CALL ///////////////////////////
/*!
 *  exampleCommand : This is a on demand api call
 *  Command: {"cmd":"example_command","payload":{"cmd_data": 234}}
 *  Emits the following JSON:
 *  {
 *   "notification" : {
 *      "value" : "example_command_response",
 *      "payload" : {
 *      "cmd_data" : 10
 *      }
 *    }
 * }
 */
bool ImplCommands::exampleCommand(const ArduinoJson::JsonObject &payload) {
    int cmd_data = payload["payload"]["cmd_data"];

    StaticJsonBuffer < 512 > notification_json_buffer;  // Notification JSON buffer
    JsonObject &json_object = notification_json_buffer.createObject();
    JsonObject &notification_object = json_object.createNestedObject("notification");
    notification_object["value"] = "example_command_response";

    JsonObject &payload_object = notification_object.createNestedObject("payload");
    payload_object["cmd_data"] = cmd_data;

    char string_response[512];
    json_object.printTo(string_response);
    dispatcher_->emit(string_response);
    return true;
}

//////////////////////// PERIODIC CALL ///////////////////////////
/*!
 *  exampleCommand2 : This is a periodic call
 *  Emits the following JSON:
 *  {
 *   "notification" : {
 *      "value" : "cmd_response",
 *      "payload" : {
        "cmd_data" : 2525
 *      }
 *    }
 * }
 */
bool ImplCommands::examplePeriodicCommand() {
    StaticJsonBuffer < 512 > notification_json_buffer;  // Notification JSON buffer
    JsonObject &json_object = notification_json_buffer.createObject();
    JsonObject &notification_object = json_object.createNestedObject("notification");
    notification_object["value"] = "periodic_example_response";

    JsonObject &payload_object = notification_object.createNestedObject("payload");
    payload_object["cmd_data"] = 2525;

    char string_response[512];
    json_object.printTo(string_response);
    dispatcher_->emit(string_response);
    return true;
}

// Blink Link LED
bool ImplCommands::blinkLinkLed() {
    link_led = !link_led;
    return true;
}
