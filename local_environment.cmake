# This file is for setting correct variables to the other components and libraries

set(TARGET_CPU EFM32GG380F1024G)
set(TARGET_CPU_ARCHITECTURE cortex-m3)

set(MBED_OS_VERSION "5.9.7")
set(MBED_OS_TARGET "EFM32GG_ONH2OHEAT")

# TARGET_DEVICE variable will be used in jlink script for flashing only.
set(TARGET_DEVICE EFM32GG380F1024)
